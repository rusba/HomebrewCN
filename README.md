# HomebrewCN
Homebrew 国内自动安装脚本

安装：
/bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"

卸载：
/bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/HomebrewUninstall.sh)"

# iOS安装cocoapods
## 1.使用本脚本安装brew
## 2.通过brew安装ruby
### https://www.jianshu.com/p/c42f2cda860e
## 3.设置ruby路径
### 安装ruby时，终端会提示设置ruby的安装路径,https://blog.csdn.net/shaobo8910/article/details/105314234/
## 4.修改gem源
### gem sources --add https://gems.ruby-china.com/ --remove https://rubygems.org/
### gem sources -l
### https://gems.ruby-china.com
### 确保只有 gems.ruby-china.com
## 5.使用sudo gem install -n /usr/local/bin cocoapods 安装cocoapods
## 6.zsh: command not found: 解决方法
### https://www.jianshu.com/p/eee571b4e97d, 
### https://blog.csdn.net/qq_23292307/article/details/79744693
## 7.解决：/System/Library/Frameworks/Ruby.framework/Versions/2.6.0/usr/bin/ruby: bad inte...
### https://www.jianshu.com/p/ad3e537931ef, 
### https://blog.csdn.net/wjm_344588756/article/details/78812186?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromBaidu-1.control&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromBaidu-1.control
## 8.pod 查看当前源/添加源
### https://www.jianshu.com/p/a18ca0c4c070, 
### https://www.cnblogs.com/kaerxifa/p/11174203.html
